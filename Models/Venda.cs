namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public string NomeVendedor { get; set; }
        public int Cpf { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Itens {get;set;}
        public DateTime Data { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}