using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;
        public VendaController(VendaContext context)
        {
            _context = context;
        }
        [HttpPost]
        public IActionResult Create (Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }       
        [HttpGet("{id}")]
        public IActionResult BuscarVenda (int id)
        {
            var venda = _context.Vendas.Find(id);
            if(venda == null)
               return NotFound();

            return Ok(venda);

        }
        [HttpPut("{id}")]
        public IActionResult AtualizarStatus (int id, Venda venda)
        {
            var VendaAtualizado = _context.Vendas.Find(id);
            if(VendaAtualizado == null)
                return NotFound();
            VendaAtualizado.Status = venda.Status;
               if (VendaAtualizado.Status == EnumStatusVenda.Aguardando_pagamento)
            {
                VendaAtualizado.Status = EnumStatusVenda.Pagamento_aprovado;
            }
             else if (VendaAtualizado.Status == EnumStatusVenda.Pagamento_aprovado)
            {
                VendaAtualizado.Status = EnumStatusVenda.Enviado_para_a_transportadora;
            } else if (VendaAtualizado.Status == EnumStatusVenda.Enviado_para_a_transportadora)
            {
                VendaAtualizado.Status = EnumStatusVenda.Entregue;
            }
            _context.Vendas.Update(VendaAtualizado);
            _context.SaveChanges();
            return Ok(VendaAtualizado);
        }
        [HttpPut("Cancelar/{id}")]
        public IActionResult Cancelar (int id, Venda venda)
        {
            var VendaAtualizado = _context.Vendas.Find(id);
            if(VendaAtualizado == null)
               return NotFound();
            VendaAtualizado.Status = EnumStatusVenda.Cancelado;
            _context.Vendas.Update(VendaAtualizado);
            _context.SaveChanges();
            return Ok(VendaAtualizado);
        }
 
    }
  
}
